# cki-jira

## Creating a release

1. Create a release MR with an update of the version number in
   `cki_jira/__init__.py`, e.g to '3.1.4'
1. Create an annotated tag with the same version prefixed with `v` and enter
   the release notes as the tag message, e.g.

   ```bash
   git tag v3.1.4 -a
   ```

   For the release notes, list the important changes and include the
   merge requests that introduced them, e.g.

   ```markdown
   - Run tests in parallel on multiple Python versions (!8)
   ```

1. Push the tag to GitLab, e.g.

   ```bash
   git push origin v3.1.4
   ```

1. Wait for the tag pipeline to finish
1. Check the resulting GitLab and PyPI releases
